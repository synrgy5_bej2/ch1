import java.util.Scanner;

public class LuasVolume {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        boolean ulang = true;
        String memproses = "\nProcessing... ";
        String inputTinggi = "Masukkan tinggi : ";
        while (ulang) {
            printMainMenu();
            System.out.println("Masukkan pilihan Anda : ");
            int pilihan1 = input.nextInt();
            switch (pilihan1){
                case 0:
                    ulang = false;
                    continue;
                case 1:
                    printShapesMenu();
                    System.out.println("Masukkan pilihan Anda : ");
                    int pilihanBidang = input.nextInt();
                    while (pilihanBidang > 4){
                        System.out.println("Pilihan invalid! Silakan input pilihan kembali. ");
                        printShapesMenu();
                        System.out.println("Masukkan pilihan Anda : ");
                        pilihanBidang = input.nextInt();
                    }
                    switch (pilihanBidang) {
                        case 0:
                            continue;
                        case 1:
                            System.out.println(
                                    "\n-------------------- \n" +
                                            "Anda memilih persegi \n" +
                                            "-------------------- \n");
                            System.out.println("Masukkan sisi : ");
                            int sPersegi = input.nextInt();
                            System.out.println(memproses);
                            int luasPersegi = sPersegi * sPersegi;
                            System.out.println("\nLuas dari persegi adalah " + luasPersegi);
                            break;
                        case 2:
                            System.out.println(
                                    "\n---------------------- \n" +
                                            "Anda memilih lingkaran \n" +
                                            "---------------------- \n");
                            System.out.println("Masukkan jari-jari : ");
                            int rLingkaran = input.nextInt();
                            System.out.println(memproses);
                            double luasLingkaran = (Math.PI) * (rLingkaran*rLingkaran);
                            System.out.println("\nLuas dari lingkaran adalah " + luasLingkaran);
                            break;
                        case 3:
                            System.out.println(
                                    "\n--------------------- \n" +
                                            "Anda memilih segitiga \n" +
                                            "--------------------- \n");
                            System.out.println("Masukkan alas : ");
                            int alasSegi3 = input.nextInt();
                            System.out.println(inputTinggi);
                            int tinggiSegi3 = input.nextInt();
                            System.out.println(memproses);
                            double luasSegi3 = (alasSegi3*tinggiSegi3)*(0.5);
                            System.out.println("\nLuas dari Segitiga adalah " + luasSegi3);
                            break;
                        case 4:
                            System.out.println(
                                    "\n---------------------------- \n" +
                                            "Anda memilih persegi panjang \n" +
                                            "---------------------------- \n");
                            System.out.println("Masukkan panjang : ");
                            int pRectangle = input.nextInt();
                            System.out.println("Masukkan lebar : ");
                            int lRectangle = input.nextInt();
                            System.out.println(memproses);
                            int luasRectangle = (pRectangle*lRectangle);
                            System.out.println("\nLuas dari Persegi Panjang adalah " + luasRectangle);
                            break;
                        default :
                            continue;
                    }
                    break;
                case 2:
                    print3DShapesMenu();
                    System.out.println("Masukkan pilihan Anda : ");
                    int pilihanBidang3D = input.nextInt();
                    while (pilihanBidang3D > 3){
                        System.out.println("Pilihan invalid! Silakan input pilihan kembali. ");
                        print3DShapesMenu();
                        System.out.println("Masukkan pilihan Anda : ");
                        pilihanBidang3D = input.nextInt();
                    }
                    switch (pilihanBidang3D) {
                        case 0:
                            continue;
                        case 1:
                            System.out.println(
                                    "\n------------------ \n" +
                                            "Anda memilih kubus \n" +
                                            "------------------ \n");
                            System.out.println("Masukkan sisi : ");
                            int sKubus = input.nextInt();
                            System.out.println(memproses);
                            int volKubus = (sKubus * sKubus) * sKubus;
                            System.out.println("\nVolume dari Kubus adalah " + volKubus);
                            break;
                        case 2:
                            System.out.println(
                                    "\n------------------ \n" +
                                            "Anda memilih balok \n" +
                                            "------------------ \n");
                            System.out.println("Masukkan panjang : ");
                            int pBalok = input.nextInt();
                            System.out.println("Masukkan lebar : ");
                            int lBalok = input.nextInt();
                            System.out.println(inputTinggi);
                            int tBalok = input.nextInt();
                            System.out.println(memproses);
                            int volBalok = (pBalok*lBalok)*tBalok;
                            System.out.println("\nVolume dari Balok adalah " + volBalok);
                            break;
                        case 3:
                            System.out.println(
                                    "\n------------------- \n" +
                                            "Anda memilih tabung \n" +
                                            "------------------- \n");
                            System.out.println("Masukkan jari-jari : ");
                            int rTabung = input.nextInt();
                            System.out.println(inputTinggi);
                            int tTabung = input.nextInt();
                            System.out.println(memproses);
                            double volTabung = (Math.PI) * (rTabung*rTabung) * tTabung;
                            System.out.println("\nVolume dari Tabung adalah " + volTabung);
                            break;
                        default:
                            continue;
                    }
                    break;
                default:
                    System.out.println("Pilihan invalid! Silakan input pilihan kembali. ");
                    continue;
            }
            System.out.println( "\n------------------------------------------\n" +
                                "Tekan apa saja untuk kembali ke menu utama");
            char c = input.next().charAt(0);
        }
        input.close();
    }

    // prosedur untuk mencetak menu utama
    private static void printMainMenu()
    {
        System.out.println(
                "\n------------------------------------- \n" +
                        "Kalkulator Penghitung Luas dan Volume \n" +
                        "------------------------------------- \n" +
                        "Menu \n" +
                        "1. Hitung Luas Bidang \n" +
                        "2. Hitung Volume \n" +
                        "0. Tutup Aplikasi \n");
    }

    // prosedur untuk mencetak menu bidang
    private static void printShapesMenu()
    {
        System.out.println(
                "\n------------------------------- \n" +
                        "Pilih bidang yang akan dihitung \n" +
                        "------------------------------- \n" +
                        "Menu \n" +
                        "1. Persegi \n" +
                        "2. Lingkaran \n" +
                        "3. Segitiga \n" +
                        "4. Persegi panjang \n" +
                        "0. Kembali \n");
    }

    // prosedur untuk mencetak menu bidang
    private static void print3DShapesMenu()
    {
        System.out.println(
                "\n---------------------------------- \n" +
                        "Pilih bidang 3D yang akan dihitung \n" +
                        "---------------------------------- \n" +
                        "Menu \n" +
                        "1. Kubus \n" +
                        "2. Balok \n" +
                        "3. Tabung \n" +
                        "0. Kembali \n");
    }
}
